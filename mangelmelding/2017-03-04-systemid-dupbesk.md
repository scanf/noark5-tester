Fjern duplisert beskrivelse av SystemID
=======================================

 ------------------  ---------------------------------
           Prosjekt  NOARK 5 Tjenestegresesnitt
           Kategori  Versjon 1.0 beta
        Alvorlighet  kommentar
       Meldingstype  trenger klargjøring
    Brukerreferanse  pere@hungry.com
        Dokumentdel  7.2.1.3
         Sidenummer  75
        Linjenummer  ?
    Innsendingsdato  ikke sendt inn
 ------------------  ---------------------------------

Beskrivelse
-----------

Det er flere beskrivelser av SystemID i objektbeskrivelsene
dokumentet, og de er litt forskjellige fra gang til gang.  Det er
bedre å ha en samlet beskrivelse av SystemID som det refereres til, og
kun skrive det som er unikt/spesielt for et gitt objekt i
objektbeskrivelsene.

FIXME nevn eksempler.

Ønsket endring
--------------

Skriv om første avsnitt fra

> En arkivenhet (se NOARK 5 v3.1 krav 5.1.2 og 5.1.3) skal kunne
> identifiseres entydig innenfor det arkivskapende organet. I et
> arkivuttrekk skal denne identifikasjonen hete systemID, og være
> entydig på tvers av alle uttrekk som organet produserer, dermed også
> på tvers av alle systemer organet benytter. Også arkivenheter som
> dupliseres i et arkivuttrekk, skal identifiseres entydig, slik at
> identiske arkivenheter har ulik systemID.

til

> FIXME henvis til SystemID-beskrivelsen, ha kun med detaljer som er
> elevant for arkivenhet.

Skriv om merknad på side 77 til å også henvise til SystemID-beskrivelse.

Respons
-------

Ingen respons fra arkivverket så langt.
