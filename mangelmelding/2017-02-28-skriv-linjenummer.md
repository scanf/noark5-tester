Linjenummer i tekstmargen vil forenkle henvisninger
===================================================

 ------------------  ---------------------------------
           Prosjekt  NOARK 5 Tjenestegresesnitt
           Kategori  Versjon 1.0 beta
        Alvorlighet  kommentar
       Meldingstype  utelatt
    Brukerreferanse  pere@hungry.com
        Dokumentdel  hele dokumentet
         Sidenummer  n/a
        Linjenummer  n/a
    Innsendingsdato  ikke sendt inn
 ------------------  ---------------------------------

Beskrivelse
-----------

Det ville være enklere å referere til bestemte deler i spesifikasjonen
hvis teksten hadde linjenummer i margen.  Da slapp en å telle avsnitt
og setninger, og kunne nøye seg med å opplyse om sidetall og
linjenummer.  Dette gjelder også JSON- og XML-eksempler, som bør kunne
refereres til med linjenummer.  Endel av disse eksemplene er i dag
tatt inn i dokumentet som bilder, hvilket gjør at en må skrive av
eksemplene med tilhørende fare for stavefeil.

Ønsket endring
--------------

Tekstmalen som brukes til spesifikasjonen bør endres slik at alle
sider har linjenummer i marken.

Et eksempel på en oppskrift for Microsoft Word 2013 finnes på
https://www.howtogeek.com/170789/how-to-number-lines-in-the-margins-in-word-2013/ .
